const mysql = require('mysql')
const pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'pokemon_app_db',
})

// Kiểm tra kết nối cơ sở dữ liệu
pool.getConnection((error, connection) => {
  if (error) {
    console.error('Connect to database fail:', error);
  } else {
    console.log('Connect to database success!');
    // Đảm bảo bạn giải phóng kết nối sau khi sử dụng nó
    connection.release();
  }
});

module.exports = pool